﻿using UnityEngine;
using System.Collections;

public class AdManager : MonoBehaviour {

	public GameObject ad01;
	public GameObject ad02;
	public GameObject ad03;
	public GameObject ad04;



	// Use this for initialization
	void Start () {
		StartCoroutine(AppearManager(Random.Range(1,4), 1f));
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Appear(GameObject target){
		Animator anim;
		anim = target.GetComponent<Animator>();

		if(anim.GetCurrentAnimatorStateInfo(0).IsName("default")){
			switch(target.name){
			case "Ad01":
				anim.SetTrigger("caseLeft_A");
				break;
			case "Ad02":
				anim.SetTrigger("caseRight_A");
				break;
			case "Ad03":
				anim.SetTrigger("caseLeft_A");
				break;
			case "Ad04":
				anim.SetTrigger("caseRight_A");
				break;

			}

			StartCoroutine(AppearManager(Random.Range(1,4), 2f));

		}

	}

	public void Disappear(GameObject target){
		Animator anim;
		anim = target.GetComponent<Animator>();

		switch(target.name){
		case "Ad01":
			anim.SetTrigger("caseLeft_D");
			break;
		case "Ad02":
			anim.SetTrigger("caseRight_D");
			break;
		case "Ad03":
			anim.SetTrigger("caseLeft_D");
			break;
		case "Ad04":
			anim.SetTrigger("caseRight_D");
			break;
		}

		StartCoroutine(AppearManager(Random.Range(1,4), 3f));
	}

	IEnumerator AppearManager(int caseNum, float waitTime){
		yield return new WaitForSeconds(waitTime);

		switch(caseNum){
			case 1:
				Appear(ad01);
				break;
			case 2:
				Appear(ad02);
				break;
			case 3:
				Appear(ad03);
				break;
			case 4:
				Appear(ad04);
				break;

		}
	}

}
