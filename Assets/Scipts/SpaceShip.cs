﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpaceShip : MonoBehaviour{

	public int HP {get;set;}
	public int MaxHP = 10;
	public Image HPUI;

	//move variable
	public Rigidbody2D rb;

	float maxSpeed = 50f;
	public float speed = 2f;

	public enum Direction{Up, Down, Right, Left, Stop};
	Vector2 moveUp = new Vector2(0,1f);
	Vector2 moveDown = new Vector2(0,-1f);
	Vector2 moveLeft = new Vector2(-1f,0);
	Vector2 moveRight = new Vector2(1f,0);

	//fire variable
	protected float dis = 0.5f;
	protected float fireTimer;
	protected bool fireInit = false;
	GameObject bulletPooler;
	public GameObject bulletPrefab;

	void Start(){
		HP = MaxHP;
		//rb = this.GetComponent<Rigidbody2D>();
		bulletPooler = GameObject.Find("ObjectPoolers").GetComponent<CreatePoolerScript>().CreatePooler(bulletPrefab, 30, true);
		GameObject.Find("GameManager").GetComponent<GameManager>().bulletPooler = bulletPooler;
	}

	public void HPChange(int num){
		HP += num;
		HPUI.fillAmount = (float)HP/MaxHP;
	}

	protected void Move(Direction dir){

		if(rb != null){
			if(dir == Direction.Up){

				if(rb.velocity.y < maxSpeed){
					rb.velocity = new Vector2(rb.velocity.x, moveUp.y * speed);
				}

			}else if(dir == Direction.Down){

				if(rb.velocity.y > -maxSpeed){
					rb.velocity = new Vector2(rb.velocity.x, moveDown.y * speed);
				}

			}

			if(dir == Direction.Left){

				if(rb.velocity.x > -maxSpeed){
					rb.velocity = new Vector2(moveLeft.x * speed, rb.velocity.y);
				}

			}else if(dir == Direction.Right){

				if(rb.velocity.x < maxSpeed){
					rb.velocity = new Vector2(moveRight.x * speed, rb.velocity.y);
				}

			}

			if(dir == Direction.Stop){
				rb.velocity = new Vector2(0, 0);
			}

		}else{
			print("no rigidbody");
		}

	}
		
	protected void Fire(float fireInterval, bool shootUp){
		if(!fireInit){
			fireTimer = fireInterval;
			fireInit = true;
		}

		fireTimer -= Time.deltaTime;
		if (fireTimer <= 0)
		{
			GameObject bulletTemp;
			bulletTemp = bulletPooler.GetComponent<ObjectPoolerScript>().GetPooledObject();
			if(shootUp){
				bulletTemp.transform.position = this.transform.position + this.transform.up * dis;
				//bulletTemp.transform.rotation = this.transform.rotation;

				//bulletTemp.GetComponent<Rigidbody2D>().gravityScale *= -1;
			}else{
				bulletTemp.transform.position = this.transform.position - this.transform.up * dis;
				//bulletTemp.transform.rotation = Quaternion.Euler(0,0,-180);

				bulletTemp.GetComponent<BulletControlScript>().speed *= -1;
			}
			bulletTemp.SetActive(true);
			//bulletTemp = Instantiate(Resources.Load("Prefabs/defaultBullet"), this.transform.position + this.transform.up * dis, this.transform.rotation) as GameObject;
			bulletTemp.GetComponent<BulletControlScript>().shooter = this.gameObject;
			//Destroy(bulletTemp, 3f);
			fireTimer = fireInterval;
		}
			
	}

	protected void ChangeSprite(Sprite spriteRef){
		this.GetComponent<SpriteRenderer>().sprite = spriteRef;
	}

}
