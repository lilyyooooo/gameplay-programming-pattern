﻿using UnityEngine;
using System.Collections;

public class Enemy_B : Enemy {

	public void Activate(){
		Fire(0.5f,false);
		MovePatternB(1,5);
	}

	void Update(){
		if(HP <= 0){
			HP = MaxHP;
			this.gameObject.SetActive(false);
		}
	}

}
