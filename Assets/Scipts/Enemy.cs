﻿using UnityEngine;
using System.Collections;

public class Enemy : SpaceShip{

	protected float moveTimer;
	protected bool moveInit = false;
	protected Direction dir;

	bool changeDir = false;

	//virtual: can be override
	//default: move left and right, move down a little bit & change dir when reach the edge/after a fixed time
	protected void MovePatternA(float fallChangeTime){
		if(!moveInit){
			moveTimer = 0.1f;
			Move(Direction.Right);
			dir = Direction.Right;
			moveInit = true;
		}

		//change direction
		//Camera.main.WorldToViewportPoint(transform.position).x > 0.85f || Camera.main.WorldToViewportPoint(transform.position).x < 0.15f
		if (changeDir)
		{
			Move(Direction.Stop);
			Move(Direction.Down);

			moveTimer -= Time.deltaTime;

			if(moveTimer <= 0){
				Move(Direction.Stop);

				if(dir == Direction.Right){
					Move(Direction.Left);
					dir = Direction.Left;
				}else if(dir == Direction.Left){
					Move(Direction.Right);
					dir = Direction.Right;
				}

				moveTimer = 0.1f;
				changeDir = false;
			}
		}

	}

	//default: move left and right, change dir when reach the edge/after a random time
	protected void MovePatternB(float min, float max){
		if(!moveInit){
			moveTimer = Random.Range(min,max);
			Move(Direction.Right);
			dir = Direction.Right;
			moveInit = true;
		}

		moveTimer -= Time.deltaTime;

		if (moveTimer <= 0 || Camera.main.WorldToViewportPoint(transform.position).x > 0.85f || Camera.main.WorldToViewportPoint(transform.position).x < 0.15f)
		{
			if(dir == Direction.Right){
				Move(Direction.Left);
				dir = Direction.Left;
			}else{
				Move(Direction.Right);
				dir = Direction.Right;
			}
			moveTimer = Random.Range(min,max);
		}

	}

	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "wall"){
			changeDir = true;
		}

		if(col.gameObject.name == "base"){
			GameObject.Find("GameManager").GetComponent<GameManager>().GameEnd();
		}
	}
}

