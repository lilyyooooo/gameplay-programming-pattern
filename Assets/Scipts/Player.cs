﻿using UnityEngine;
using System.Collections;

public class Player : SpaceShip {

	public KeyCode keyUp = KeyCode.W;
	public KeyCode keyDown = KeyCode.S;
	public KeyCode keyLeft = KeyCode.A;
	public KeyCode keyRight = KeyCode.D;

	public KeyCode keyFire = KeyCode.J;

	void Update(){
		FireControl();
	}

	// Update is called once per frame
	void FixedUpdate () {
		MoveControl();
	}

	void MoveControl(){
		if(Input.GetKey(keyUp)){

			Move(Direction.Up);

		}else if(Input.GetKey(keyDown)){

			Move(Direction.Down);

		}
		else{

			rb.velocity = new Vector2(rb.velocity.x,0);

		}

		if(Input.GetKey(keyLeft)){

			Move(Direction.Left);

		}else if(Input.GetKey(keyRight)){

			Move(Direction.Right);

		}
		else{

			rb.velocity = new Vector2(0,rb.velocity.y);

		}

	}

	void FireControl(){
//		if(Input.GetKeyDown(keyFire)){
//			Fire(0,true);
//		}

		if(Input.GetMouseButtonDown(0)){
			Fire(0,true);
		}
	}

	public void Reset(){
		HP = MaxHP;
		HPUI.fillAmount = (float)HP/MaxHP;
	}

}
