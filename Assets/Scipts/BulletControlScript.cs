﻿using UnityEngine;
using System.Collections;

public class BulletControlScript : MonoBehaviour {

	Rigidbody2D rb;
	Renderer rd;

	public float speed = 1f;
	public int damage = 1;

	public GameObject shooter;

	// Use this for initialization
	void Start () {
		rb = this.GetComponent<Rigidbody2D>();
		rd = this.GetComponent<SpriteRenderer>();
	}

	//wait it enable first
	void LateUpdate(){
//		if(!rd.isVisible){
//			this.gameObject.SetActive(false);
//		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		rb.velocity = new Vector3(0,1,0) * speed;
	}


	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "wall"){
			speed *= -1;
		}

		if(shooter.tag == "enemy"){
			if(col.gameObject.tag == "player"){
				col.gameObject.GetComponent<Player>().HPChange(-damage);
				this.gameObject.SetActive(false);
			}
		}else if(shooter.tag == "player"){
			if(col.gameObject.tag == "enemy"){
				col.gameObject.GetComponent<Enemy>().HPChange(-damage);
				this.gameObject.SetActive(false);
			}
		}

//		else if(shooter.tag == "bullet"){
//			this.gameObject.SetActive(false);
//		}

	}


	void OnCollisionEnter2D(Collision2D col){
//		if(col.gameObject.tag == "wall"){
//			speed *= -1;
//		}
//
//		if(shooter.tag == "enemy"){
//			if(col.gameObject.tag == "player"){
//				col.gameObject.GetComponent<Player>().HPChange(-damage);
//				this.gameObject.SetActive(false);
//			}
//		}else if(shooter.tag == "player"){
//			if(col.gameObject.tag == "enemy"){
//				col.gameObject.GetComponent<Enemy>().HPChange(-damage);
//				this.gameObject.SetActive(false);
//			}
//		}else 

		if(col.gameObject.tag == "bullet"){
			this.gameObject.SetActive(false);
		}
	}

	void OnEnable(){
		StartCoroutine(SetDisable(8f));
	}

	IEnumerator SetDisable(float waitTIme){
		yield return new WaitForSeconds(waitTIme);
		//reset
		speed = Mathf.Abs(speed);
		this.gameObject.SetActive(false);
	}

	public void DisableImmediately(){
		speed = Mathf.Abs(speed);
		this.gameObject.SetActive(false);
	}
		
}
