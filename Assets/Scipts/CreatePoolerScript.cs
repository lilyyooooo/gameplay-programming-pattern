﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreatePoolerScript : MonoBehaviour {

	List<GameObject> existPoolers = new List<GameObject>();

	public GameObject CreatePooler(GameObject prefab, int defaultAmount, bool willGrow){

		foreach (GameObject existPooler in existPoolers) {
			//pooler exsit
			if (existPooler.GetComponent<ObjectPoolerScript> ().pooledObject == prefab) {
				return existPooler;
			}
		}
			
		//pooler not exsit, create a new one
		GameObject pooler = Instantiate (Resources.Load ("Prefabs/ObjectPooler")) as GameObject;
		pooler.transform.parent = GameObject.Find ("ObjectPoolers").transform;

		ObjectPoolerScript poolerControlScript = pooler.GetComponent<ObjectPoolerScript> ();
		poolerControlScript.pooledObject = prefab;
		poolerControlScript.pooledAmount = defaultAmount;
		poolerControlScript.willGrow = willGrow;

		existPoolers.Add (pooler);

		return pooler;
	}
}
