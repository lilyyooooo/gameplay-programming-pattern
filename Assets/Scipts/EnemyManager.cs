﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : BasicManager {

	public List<GameObject> enemies = new List<GameObject>();

	public GameObject enemyPrefabA;
	public GameObject enemyPrefabB;

	public GameObject enemyAPooler;
	public GameObject enemyBPooler;

	float timer = 0;
	public float nextWaveTime = 30f;

	public int level {get;set;}

	GameManager gameManagerScript;

	// Use this for initialization
	void Start () {
		gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();

		enemyAPooler = GameObject.Find("ObjectPoolers").GetComponent<CreatePoolerScript>().CreatePooler(enemyPrefabA, 5, true);
		enemyBPooler = GameObject.Find("ObjectPoolers").GetComponent<CreatePoolerScript>().CreatePooler(enemyPrefabB, 5, true);

//		level = 0;
//		LevelLoad();

	}

	void Update(){
		if(gameManagerScript.gameStarted){
			
			if(level == 1){
				LevelLoad();
			}

		}
	}

	void FixedUpdate () {
		if(gameManagerScript.gameStarted){

			if(enemies.Count > 0){
				for(int i = 0; i < enemies.Count; i++){
					if(!enemies[i].activeInHierarchy){
						enemies.Remove(enemies[i]);
					}else{
						enemies[i].GetComponent<Enemy_A>().Activate();
					}
				}
			}else{
				level += 1;
				LevelLoad();
			}

		}

	}


	public void LevelLoad(){
		switch(level){
		case 0:
			print("level 0");
			EnemyWaveA(1);
			break;
		case 1:
			print("level 1");
			timer -= Time.deltaTime;
			if(timer <=0){
				EnemyWaveA(1);
				timer = nextWaveTime;
			}
			break;
		}

	}

	void EnemyWaveA(int num){
		int amount = num;
		for(int i = 0; i < amount; i++){
			GameObject tempObj;
			tempObj = enemyAPooler.GetComponent<ObjectPoolerScript>().GetPooledObject();
			//tempObj = Instantiate(enemyPrefabA);
			tempObj.transform.position += new Vector3(0, i*2, 0);
			enemies.Add(tempObj);
			tempObj.SetActive(true);
			//tempObj.GetComponent<Enemy_A>().Activate();
		}
	}
}
