﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPoolerScript : MonoBehaviour {
	//this is a universal script using to create object pool

	public GameObject pooledObject;
	public int pooledAmount = 20;
	//will this pooler expand dynamically
	public bool willGrow = true;

	public List<GameObject> pooledObjects;

	void Start () {
		pooledObjects = new List<GameObject> ();
		for (int i = 0; i < pooledAmount; i++) {
			GameObject obj = (GameObject)Instantiate (pooledObject);
			//put it under the pooler
			obj.transform.parent = this.transform;
			obj.SetActive (false);
			pooledObjects.Add (obj);
		}
	}

	//could be access from other script
	public GameObject GetPooledObject(){
		for (int i = 0; i < pooledObjects.Count; i++) {
			//if it's an inactivated pooledObject, if it is, return this to whom want it
			if (!pooledObjects [i].activeInHierarchy) {
				return pooledObjects [i];
			}
		}

		//if there isn't any more inactivated object left in the pooler, add a new one
		if (willGrow) {
			GameObject obj = (GameObject)Instantiate (pooledObject);
			obj.transform.parent = this.transform;
			pooledObjects.Add (obj);
			return obj;
		}

		//if can't do both
		return null;
	}
}
