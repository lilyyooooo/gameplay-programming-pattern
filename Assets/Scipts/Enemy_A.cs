﻿using UnityEngine;
using System.Collections;

public class Enemy_A : Enemy {

	public void Activate(){
		Fire(0.5f,false);
		MovePatternA(4f);
	}

	void Update(){
		if(HP <= 0){
			this.gameObject.SetActive(false);
		}
	}

	//reset
	void OnEnable(){
		HP = MaxHP;
		HPUI.fillAmount = (float)HP/MaxHP;
		this.transform.position = new Vector3 (0, 6, 0);
		moveInit = false;
	}

}
