﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public Text gameUI;
	Player player;

	public bool gameStarted {get;set;}

	EnemyManager enemyManagerScript;
	public GameObject bulletPooler;

	// Use this for initialization
	void Start () {
		Screen.SetResolution(450,800,false);
		player = GameObject.Find("player").GetComponent<Player>();
		enemyManagerScript = GameObject.Find("EnemyManager").GetComponent<EnemyManager>();

		gameStarted = false;
		gameUI.text = "R To Start"; 
	}
	
	// Update is called once per frame
	void Update () {
		if(player.HP <= 0){
			GameEnd();
		}

		if(Input.GetKeyDown(KeyCode.R)){
			StartGame();
		}
	}

	public void GameEnd(){
		gameUI.text = "GameOver!";
	}

	void StartGame(){
		gameUI.text = ""; 
		gameStarted = true;

		foreach(Transform child in bulletPooler.transform){
			if(child.gameObject.activeInHierarchy){
				child.GetComponent<BulletControlScript>().DisableImmediately();
			}
		}

		for(int i = 0; i < enemyManagerScript.enemies.Count; i ++){
			enemyManagerScript.enemies[i].SetActive(false);
		}
		enemyManagerScript.enemies.Clear();

		player.GetComponent<Player>().Reset();

		enemyManagerScript.level = 0;
		enemyManagerScript.LevelLoad();
	}
}
